import json
import websocket


def on_close(ws):
    print('close')


def on_message(ws, message):
    closes, highs, lows, last_price, mean_prices = [], [], [], [], []
    binance_data = json.loads(message)['k']  # Словарь с данными биржы in real time
    is_candle_closed, close, btc_price_high, btc_price_low = binance_data['x'], binance_data['c'], binance_data['h'], \
                                                             binance_data['l']

    if is_candle_closed:
        print(btc_price_high, btc_price_low)
        closes.append(float(close))
        highs.append(float(btc_price_high))
        lows.append(float(btc_price_low))
        mean_prices.append((float(btc_price_high) + float(btc_price_low)) / 2)
        last_price = closes[-1]
        print(highs)
        print(f'свечка?: {last_price}')

socket = 'wss://stream.binance.com:9443/ws/btcusdt@kline_1m'
ws = websocket.WebSocketApp(socket, on_message=on_message, on_close=on_close)
ws.run_forever()
