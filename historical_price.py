from header import *


class historical_data_analysis: # (QtWidgets.QApplication)

    def __init__(self):
        tp = 14
        self.parse_data()
        self.aroon(tp)
        self.time_to_sell_buy()
        self.use_buy_or_sell()
        # self.journal()
        self.plot_figs()
    # KO stock price data ->
    # https://finance.yahoo.com/quote/KO/history?period1=1466553600&period2=1624320000&interval=1d&filter=history&frequency=1d&includeAdjustedClose=true

    def parse_data(self):
        data = pd.read_csv('NOK.csv', ',')
        self.high = data['High'][0:2000].tolist()
        self.low = data['Low'][0:2000].tolist()
        self.mean_pr = ((np.array(self.high) + np.array(self.low)) / 2).tolist()

    def aroon(self, tp):
        #https://www.youtube.com/watch?v=FbNYNRjalEc&ab_channel=sentdex
        self.aroon_up, self.aroon_down = [], []
        for i in range(len(self.high)):
            how_long_high = self.high[i:i+tp].index(max(self.high[i:i+tp]))# i*tp -
            how_long_low = self.high[i:i+tp].index(min(self.high[i:i+tp])) #i*tp -
            a_down = 100*(tp - how_long_high)/tp
            a_up = 100*(tp - how_long_low)/tp
            self.aroon_up.append(a_up)
            self.aroon_down.append(a_down)
        print(len(self.aroon_up), len(self.aroon_down))

    def time_to_sell_buy(self):
        self.time_buy = []
        for i in range(len(self.aroon_up)):
            if self.aroon_up[i] > 80 and self.aroon_down[i] < 20: # signal need to buy
                self.time_buy.append(i)

        # for i in range(len(self.time_buy)): # buy
        #     if self.in_cash > self.mean_pr[i]:
        #         self.buyed_stock_num = np.floor(self.in_cash/self.mean_pr[i]) # self.mean_pr[i]  <- стоимость акции во время time_u
        #         self.invested = self.buyed_stock_num * self.mean_pr[i]
        #         self.in_cash -= self.invested
        #
        # for i in range(len(self.aroon_up)): # sell
        #     if self.invested > self.mean_pr[i]:
        #         if self.aroon_up[i] > 90 and self.aroon_down[i] < 10: # signal need to sell
        #             self.time_sell = i # time_u <- время при котором была совершена продажа акций
        #             self.sell_st_price = self.mean_pr[i]  # self.mean_pr[i]  <- стоимость акции во время i
        #             self.invested = self.buyed_stock_num*self.mean_pr[i]
        #             self.in_cash += self.invested
        #             self.invested = 0

        self.time_sell = []
        for i in range(len(self.aroon_up)):
            if self.aroon_up[i] < 20 and self.aroon_down[i] > 80: # signal need to sell
                self.time_sell.append(i)

        self.tr_time_buy = [self.time_buy[0]]
        self.tr_time_sell = []

        for i in range(len(self.time_sell)):
            if self.tr_time_buy[-1] < self.time_sell[i]:
                self.tr_time_sell.append(self.time_sell[i])
                for j in range(len(self.time_buy)):
                    if self.tr_time_sell[-1] < self.time_buy[j]:
                        self.tr_time_buy.append(self.time_buy[j])
                        break

        print(f'Время покупки: {self.tr_time_buy}')
        print(f'Время продажи: {self.tr_time_sell}')

    def use_buy_or_sell(self):
        # ФУНКЦИЯ, КОТОРАЯ ГОВОРИТ КОГДА BUY OR SELL
        # parce balance from binance
        self.invested = 0
        self.in_cash = 1000
        self.net_worth = self.in_cash + self.invested  # in usd
        # ЗАВИСИМОСТЬ net_worth от времени
        self.income = self.net_worth - 1000

        for i in self.tr_time_buy: # buy
            self.buyed_stock_num = np.floor(self.in_cash/self.mean_pr[i]) # self.mean_pr[i]  <- стоимость акции во время time_u
            self.invested = self.buyed_stock_num * self.mean_pr[i]
            self.in_cash -= self.invested
            print(f'Инвестировано: {self.invested}, Цена купленных акций: {self.mean_pr[i]}, Количество купленных акций: {self.buyed_stock_num}')
            for j in self.tr_time_sell: #sell
                self.sell_st_price = self.mean_pr[j]  # self.mean_pr[i]  <- стоимость акции во время i
                self.invested = self.buyed_stock_num*self.mean_pr[j]
                self.in_cash += self.invested
                self.invested = 0
                self.tr_time_sell.pop(0)
                inc_koef = ((self.in_cash/1000) / (self.mean_pr[j]/self.mean_pr[0]))
                print(f'Наличкой: {self.in_cash}, Цена проданных акций: {self.mean_pr[j]}, Заработано за продажу акций: {self.mean_pr[i]*self.buyed_stock_num}, Коэффициент прибыльности: {inc_koef}')
                break

    # if buy than sell | if sell than buy
    # def journal(self):
    #
    #     time = 0
    #     for i in range(len(self.mean_pr)):
    #         time +=1
    #         tf_j = {'Время': time, 'Доход': self.income, 'Всего денег': self.net_worth, 'Денег в акциях': self.invested, 'Денег в кэше': self.in_cash,
    #             }
    #         # print(tf_j)

    def plot_figs(self):
        plot_stock = plt.figure(1)
        plt.plot(self.mean_pr)
        # fig, ax = plt.subplots()
        plot_aroon = plt.figure(2)
        plt.plot(self.aroon_up, label = 'Need to BUY %')
        plt.plot(self.aroon_down, label = 'Need to SELL %')
        # ax.plot(res[0], label = 'Need to BUY %')
        # ax.plot(res[1], label = 'Need to SELL %')
        plt.legend()
        plt.show()







profit = check_profit()